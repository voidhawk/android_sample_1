package org.voidhawk.onesignal;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.TextView;
import android.widget.Toast;

import com.onesignal.OneSignal;

public class MainActivity extends AppCompatActivity {

    private static final String LAUNCH_SCHEME_INIT = "onesignalinit";
    //private static final String LAUNCH_FROM_TOUR_CREATE = "onesignalinit";
    //private static final String LAUNCH_FROM_TOUR_UPDATE = "onesignalinit";
    //private static final String LAUNCH_FROM_REMOVE_DEVICE = "onesignalinit";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent intent = getIntent();
        String scheme = intent.getScheme();
        if(intent != null && scheme != null && intent.getScheme().equals(LAUNCH_SCHEME_INIT)){


            Uri dataUri = intent.getData();
            String appId = dataUri.getQueryParameter("appId");
            String uniqueReqPrefix = dataUri.getQueryParameter("uniqueReqPrefix");
            String uniqueIdentifier = dataUri.getQueryParameter("uniqueIdentifier");

            String dataString = intent.getDataString();
            Bundle myBundle = intent.getExtras();

            Toast.makeText(this, LAUNCH_SCHEME_INIT, Toast.LENGTH_SHORT).show();
            /*
            Bundle bundle = intent.getExtras();
            if(bundle != null){
                String app_id = bundle.getString("app_id");
                String uniqueReqPrefix = bundle.getString("uniqueReqPrefix");
                String uniqueIdentifier = bundle.getString("uniqueIdentifier");

            }
            */
            this.onInitOneSignal(appId, uniqueReqPrefix, uniqueIdentifier);
        }else{
            Toast.makeText(this, intent.getAction(), Toast.LENGTH_SHORT).show();
            onStartOneSignal();
            //launchInfo.setText("Normal application launch");
        }
    }

    /*
    Diese Methode soll in beiden Fällen aufgerufen werden,
    - wenn die App schon gestartet ist
    -
     */
    protected void onStartOneSignal() {

        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .setNotificationReceivedHandler(new DAveNotificationHandler(this))
                .setNotificationOpenedHandler( new DAveNotOpenedHandler())
                .init();

        // Call syncHashedEmail anywhere in your app if you have the user's email.
        // This improves the effectiveness of OneSignal's "best-time" notification scheduling feature.
        // OneSignal.syncHashedEmail(userEmail);
    }

    protected void onInitOneSignal(String app_id, String uniqueReqPrefix, String uniqueIdentifier) {

        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .setNotificationReceivedHandler(new DAveNotificationHandler(this))
                .setNotificationOpenedHandler( new DAveNotOpenedHandler())
                .init();

        OneSignal.sendTag(uniqueReqPrefix, uniqueIdentifier);

        // Call syncHashedEmail anywhere in your app if you have the user's email.
        // This improves the effectiveness of OneSignal's "best-time" notification scheduling feature.
        // OneSignal.syncHashedEmail(userEmail);
    }
}
