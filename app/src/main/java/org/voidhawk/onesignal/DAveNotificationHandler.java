package org.voidhawk.onesignal;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;

import com.onesignal.OSNotification;
import com.onesignal.OSNotificationPayload;
import com.onesignal.OneSignal;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by JST on 04.08.2017.
 */

class DAveNotificationHandler implements OneSignal.NotificationReceivedHandler {

    private Context ctx = null;
    public DAveNotificationHandler(Context parentContext) {

        ctx = parentContext;
    }
    @Override
    public void notificationReceived(OSNotification notification) {

        //Intent für die Activities triggern
        int notificationId = notification.androidNotificationId;
        OSNotificationPayload payload = notification.payload;
        JSONObject data = payload.additionalData;
        try {
            JSONArray notificationPayLoad = data.getJSONArray("notificationPayLoad");
            JSONObject o = notificationPayLoad.getJSONObject(0);
            String scemid = o.getString("scemid");
            String topicType = data.getString("topicType");

            if(topicType.equals("TOUR_CREATE")){

            }
            else if(topicType.equals("TOUR_UPDATE")){

                Intent tourUpdateIntent = new Intent();
                tourUpdateIntent.setClass(this.ctx, TourUpdateHandlerActivity.class);
                tourUpdateIntent.putExtra("SCEMID", scemid);
                tourUpdateIntent.putExtra("TopicType", topicType);
                tourUpdateIntent.setAction("GanzToll");
                this.ctx.startActivity(tourUpdateIntent);
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
        // data = {"notificationDescription":{"ident":"04e7e344-ecf3-4b05-b19a-061104a15242","appID":"be00e13a-c405-4f93-81e2-6451e91b89f6"},"notificationType":"PUSH_MOBILE","source":"EM-2215-17","scemid":"B9UTYO5WE9","subscriptionID":"f5d1a579b43545bd98e8a7c500d1deb5","topicType":"TOUR_UPDATE"}
        int i = 42;
    }
}
