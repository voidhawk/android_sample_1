package org.voidhawk.onesignal;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class TourUpdateHandlerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tour_update_handler);

        TextView scemid = (TextView)findViewById(R.id.scemid);
        TextView topicType= (TextView)findViewById(R.id.topicType);

                Intent intent = getIntent();
        String scheme = intent.getScheme();
        if (intent != null) {
            String action = intent.getAction();
            if (action.equals("GanzToll")) {
                int i = 42;
                Bundle extras = intent.getExtras();
                topicType.setText(extras.getString("TopicType"));
                scemid.setText(extras.getString("SCEMID"));
            }
            else {
                int i = 42;
            }
        }

    }
}
